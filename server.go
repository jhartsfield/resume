package main

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func home(w http.ResponseWriter, req *http.Request) {
	resume, err := ioutil.ReadFile("./resume.html")
	if err != nil {
		log.Fatal(err)
	}
	io.WriteString(w, string(resume))
}

func main() {
	var ip string = "10.0.0.5"
	var port string = "6900"
	http.HandleFunc("/", home)
	log.Println("Server started @ " + ip + ":" + port)
	log.Fatal(http.ListenAndServe(ip+":"+port, nil))
}
